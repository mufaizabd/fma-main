@extends('frontend.layouts.blocks')



@php ($attrs = [
    'class' => 'block bg-contrast',
    'containerClass' => 'py-4'
])

@section('main')

    <!-- ./Support Form -->
<x-utils.container class="{{ $class ?? '' }}" container-class="{{ $containerClass ?? '' }}">
    <div class="row align-items-center">
        <div class="col-md-6">
            <div class="device iphone-x">
                <div class="screen">
                    <img src="{{ asset('img/screens/app/1.png') }}" class="img-responsive" alt="">
                </div>
                <div class="notch"></div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="section-heading">
                <h2><span class="bold">@Lang('Maklumat Untuk Sebut Harga')</span></h2>
               
            </div>
            <!-- Basic form inputs -->
            <div class="card card-clean mb-4 shadow-box">
                <div class="card-header bg-contrast">
                    <h5 class="bold">Maklumat kenderaan</h5>
                </div>

                <div class="card-body">
                    <form action=";" class="cozy">
                        <div class="form-group">
                            <label for="exampleFormControlTextareaBase" class="form-label">No. Plate Kendereaan</label>
                            <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Masukkan Plate kereta Anda">
                        </div>

                        <div class="form-group">
                            <label  class="form-label">No.IC Pemilik Kenderaan</label>
                            <input type="text" class="form-control" placeholder="Masukkan No.IC pemilik kenderaan">
                        </div>

                        <div class="form-group">
                            <label class="form-label">No.Poskod</label>
                            <input type="text" class="form-control" placeholder="Masukkan Poskod anda">
                        </div>

                        <div class="form-group">
                            <label  class="form-label">No.Tel</label>
                            <input type="text" class="form-control" placeholder="Masukkan No.tel anda">
                        </div>

                        <div class="form-group">
                            <label  class="form-label">Email</label>
                            <input type="email" class="form-control" placeholder="Masukkan Email anda">
                        </div>

                        <div class="col-sm-12 d-grid">
                            <button data-loading-text="Sending..." type="submit" class="btn btn-rounded btn-primary">@Lang('Dapatkan Sebut Harga')</button>
                        </div>
        
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</x-utils.container>


@endsection
