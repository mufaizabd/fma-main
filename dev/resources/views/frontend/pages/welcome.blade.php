@php
    $banners = [
        [ "title" => "The Headphones Collection", "description" => "Discover our selection of the best Headphones", "badge" => "What you were waiting for?", "bg" => [ "img" => "blue-headphones", "color" => "rgb(0, 157, 206)" ] ],
        [ "title" => "Shop on the Go", "description" => "Get the best of our store at your fingertips", "badge" => "Download the App", "bg" => [ "img" => "app-deals", "color" => "rgb(92, 216, 217)" ] ],
        [ "title" => "What's makes you happy", "description" => "We have all the products to make your life easier", "badge" => "Enjoy your world", "bg" => [ "img" => "happy-girl", "color" => "rgb(240, 197, 87)" ] ],
        [ "title" => "Best performing products", "description" => "We have what you're looking for in all tech industry", "badge" => "Get them all", "bg" => [ "img" => "enjoy", "color" => "rgb(226, 162, 113)" ] ]
    ];
@endphp

@extends('frontend.layouts.pages')

@section('title', __('Laman Utama'))

@section('content')

    @include ("frontend.pages.shop.index.heading", $banners)
    @include ("frontend.pages.shared.3col-icons")
    <x-utils.divider color="contrast" />
    @include ("frontend.pages.app-landing.popping-highlight")
    @include ("frontend.blocks.sliders.1")
    @include ("frontend.blocks.testimonials.1", [ "class" => "bg-light b-t edge bottom-right pt-0" ])
    @include ("frontend.pages.app-landing.download")
    @include ("frontend.pages.shared.do-business")
    <x-utils.container class="bg-light border-top">
    @include ("frontend.blog.post._recommended")
    </x-utils.container>

@endsection

@section('footer')
    @include ("frontend.pages.welcome.footer")
    
@endsection
