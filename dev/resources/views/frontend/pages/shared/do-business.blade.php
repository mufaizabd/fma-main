@php ($cards = [
    [ "img" => "time", "title" => "Medical", "animation" => "fade-up" ],
    [ "img" => "done", "title" => "Kebakaran", "animation" => "fade-up" ],
    [ "img" => "grow", "title" => "Group Takaful", "animation" => "fade-up" ],
    [ "img" => "goals", "title" => "Personal Accident", "animation" => "fade-up" ],
    [ "img" => "time", "title" => "Contractor All Risk" , "animation" => "fade-up" ],
    [ "img" => "done", "title" => "Liability", "animation" => "fade-up" ],
    [ "img" => "grow", "title" => "Kecurian", "animation" => "fade-up" ],
    [ "img" => "goals", "title" => "Pendidikan", "animation" => "fade-up" ],
    [ "img" => "time", "title" => "Motor", "animation" => "fade-up" ],
    [ "img" => "done", "title" => "Marine", "animation" => "fade-up" ],
    [ "img" => "grow", "title" => "Performance  Bond", "animation" => "fade-up" ],
    [ "img" => "goals", "title" => "All Risk", "animation" => "fade-up" ]
])
<!-- ./Features - hover animated -->
<section class="section bg-light bg-light-gradient ">
    <div class="container bring-to-front">
        <div class="section-heading text-center">
            <!-- <h5 class="text-primary bold small text-uppercase">Let's do business</h5> -->
            <h2 class="bold">Perkhidmatan & Servis <span style="color: #53953D";>FMA TAKAFUL</span></h2>
        </div>

        <div class="row gap-y">
            @foreach ($cards as $card)
            <div class="col-lg-4">
                <div class="rounded bg-contrast shadow-box image-background off-left-background lift-hover p-4 ps-6 ps-md-9"
                    style="background-image: url({{ asset("img/lcards/{$card['img']}.svg") }})">
                    <h3>{{ $card['title'] }}</h3>
                    <p class="text-secondary mb-0">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error eveniet
                        nihil perspiciatis quia quidem quod ratione sapiente sint?
                    </p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
