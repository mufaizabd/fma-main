@php ($elements = [
    [ "icon" => "image", "title" => "Tuntutan Panggilan 24 Jam", "desc" => "Panggilan jamin dijawab" ],
    [ "icon" => "sliders", "title" => "Khidmat Tunda Percuma", "desc" => "Khidmar tunda tanpa had (24 Jam)"],
    [ "icon" => "target", "title" => "Insurance Aktif Segera", "desc" => "Khidmat tanpa had (24 Jam)"],
    [ "icon" => "target", "title" => "Khidmat Penghantaran Roadtax", "desc" => "Polisi dan roadtax akan di hantar ke pintu rumah"],
    [ "icon" => "target", "title" => "Perkhidmatan Peguam Disediakan", "desc" => "Kes melibatkan kenderaan"],
    [ "icon" => "target", "title" => "Tiada Kos Tersembunyi", "desc" => "Harga dinyatakan mengikut kadar bayaran"]

])
<!-- ./Design features -->
<x-utils.container id="features" class="{{ $class ?? '' }}">
    <div class="section-heading mb-6 text-center">
        <!-- <h5 class="text-primary bold small text-uppercase">Design better</h5> -->
        <h2 class="bold">Kenapa <span style="color: #53953D";>FMA TAKAFUL</span> Menjadi Pilihan Ramai?</h2>
    </div>

    <div class="row gap-y text-center text-md-left">
        @foreach ($elements as $element)
        <div class="col-md-4">
            <div class="card shadow-hover lift-hover">
                <div class="card-body">
                    <i data-feather="{{ $element['icon'] }}" width="36" height="36" class="stroke-primary"></i>
                    <h5 class="bold mt-3">{{ $element['title'] }}</h5>
                    <p class="bold">{{ $element['desc'] }}</p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</x-utils.container>
