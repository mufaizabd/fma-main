<x-utils.divider color="darker" />

<footer class="site-footer section text-center bg-darker text-contrast">
    <div class="container pb-4">
        <div class="section-heading">
            <!-- <i class="fa fa-rocket fa-2x"></i> -->
            <!-- <h2 class="bold mb-5 text-contrast">@Lang('Ready to get started?')</h2> -->
            

            <p class="handwritten highlight font-md mb-3">@Lang('Dapatkan Sebut harga Percuma Anda Sekarang!!!')</p>

            <a  href="" target="_blank" class="btn btn-lg btn-contrast btn-rounded mt-3 px-4 mb-3">
                @Lang('Dapatkan Sebut Harga')
            </a>

            <p class="lead">
                @Lang('Fma Takaful (Fauzee Mustaffa & Associates) adalah syarikat takaful & Insurans #1 Malaysia 
                yang sah berdaftar. Fma Takaful menyediakan laman ini bagi menyediakan perkhidmatan dan servis dari 
                pelbagai aspek bagi seluruh Malaysia termasuk Sabah, Sarawak & Singapore')
            </p>
        </div>

        <hr class="mt-5">
        <div class="row align-items-center">
    
            <div class="col-md-6 text-center mx-auto">
                
                
            <img src="{{ asset('img/logo-light.png') }}" alt="" class="logo">
                <p>
                @Lang('Waktu Bekerja | ISNIN – AHAD | 09:00 AM – 06:00 PM')
            </p>
            <nav class="nav justify-content-center justify-content">
                @rtl
                    <x-utils.link class="btn btn-circle btn-sm btn-light ms-3 " icon="fab fa-facebook" />
                    <x-utils.link class="btn btn-circle btn-sm btn-light ms-3 " icon="fab fa-twitter" />
                    <x-utils.link class="btn btn-circle btn-sm btn-light " icon="fab fa-instagram" />
                @else
                    <x-utils.link class="btn btn-circle btn-sm btn-light me-3 " icon="fab fa-facebook" />
                    <x-utils.link class="btn btn-circle btn-sm btn-light me-3 " icon="fab fa-twitter" />
                    <x-utils.link class="btn btn-circle btn-sm btn-light " icon="fab fa-instagram" />
                @endrtl
            </nav>
                <p class="mt-2 mb-0 text-muted small">© 2023
                    <a class="brand bold" target="_blank" href="https://5studios.net">FMA TAKAFUL</a>. @Lang('All Rights Reserved')
                </p>

            </div>
        </div>
        <div class="col-md-8">
            
        </div>
    </div>
</footer>
