@extends('frontend.layouts.auth')

@section('title', __('Log Masuk'))

@section('content')
    <div class="mb-5">
        <h1 class="bold text-dark my-0">@lang('Log Masuk')</h1>
        <p class="text-muted mt-0 mb-4 mb-md-6">@lang("Belum ada akaun?")
            <x-utils.link :href="route('frontend.auth.register')" class="text-primary bold" :text="__('Daftar di Sini')" />
        </p>
    </div>

    <x-forms.post :action="route('frontend.auth.login')">
        <div class="mb-4">
            <label for="email" class="form-label">@lang('Alamat Email')</label>
            <input type="email" name="email" id="email" class="form-control rounded-pill" placeholder="{{ __('Alamat email anda') }}" value="{{ old('email') }}" maxlength="255" required autofocus autocomplete="email" />
        </div>

        <div class="mb-4">
            <label for="password" class="form-label">@lang('Kata Laluan')</label>
            <input type="password" name="password" id="password" class="form-control rounded-pill" placeholder="{{ __('Masukkan Kata Laluan') }}" maxlength="100" required autocomplete="current-password" />
        </div>

        <div class="mb-4">
            <input name="remember" id="remember" class="form-check-input" type="checkbox" {{ old('remember') ? 'checked' : '' }} />

            <label class="form-check-label" for="remember">
                @lang('Ingat Maklumat')
            </label>
        </div>

        @if(config('boilerplate.access.captcha.login'))
            <div class="row">
                <div class="col">
                    @captcha
                    <input type="hidden" name="captcha_status" value="true" />
                </div><!--col-->
            </div><!--row-->
        @endif

        <div class="d-flex align-items-center justify-content-between">
            <x-utils.link :href="route('frontend.auth.password.request')" class="small" :text="__('Lupa Kata Laluan?')" />

            <button class="btn btn-primary rounded-pill px-4" type="submit">
                @lang('Log Masuk')
                <i class="fas fa-long-arrow-alt-right ms-2"></i>
            </button>
        </div>
    </x-forms.post>
@endsection
