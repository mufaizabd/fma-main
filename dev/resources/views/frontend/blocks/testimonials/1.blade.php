@php($posts = [
    [ "gravity" => 'south', "title" => 'Welcome to Dashcore', "description" => "Discover how to get started with DashCore now", "href" => "https://www.youtube.com/watch?v=EykWcFEtFqo" ],
    [ "gravity" => 'east', "title" => 'Customizing theme', "description" => "Learn how to fit the theme to your own needs", "href" => "https://www.youtube.com/watch?v=MXghcI8hcWU" ],
    [ "gravity" => 'north', "title" => 'Using the API', "description" => "Integrating the API with your new template", "href" => "https://www.youtube.com/watch?v=HLG_s9b2Uuw" ]
])
<!-- ./Testimonials -->
<x-utils.container class="{{ $class ?? '' }}" container-class="swiper-center-nav {{ $containerClass ?? '' }}">
    <div class="section-heading text-center">
        <span class="text-secondary shadow rounded-pill border py-2 px-4 bold text-dark small">Over 3,000 customers rely on DashCore</span>
        <h2 class="bold mt-3">Apa Kata Pelanggan <span style="color: #53953D";>FMA TAKAFUL</span></h2>
    </div>
    <div class="row text-center">
        <div class="col-md-6 text-center mx-auto">
            <div class="row gap-y">
                @foreach ($posts as $post)
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card bg-dark border-0">
                        <a href="{{ $post['href'] }}"
                            class="modal-popup mfp-iframe overlay gradient gradient-blue-purple alpha-3 p-6 image-background cover"
                            style="background-image: url(https://picsum.photos/350/200/?random&gravity={{ $post['gravity'] }})"
                            data-effect="mfp-fade" data-type="iframe">
                            <div class="content text-center">
                                <i data-feather="play" width="48" height="48" stroke-width="1" class="stroke-contrast"></i>
                            </div>
                        </a>

                        <div class="card-body">
                            <h4 class="card-title text-contrast">{{ $post['title'] }}</h4>

                            <p class="card-text text-muted">{{ $post['description'] }}.</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="col-md-6 text-center mx-auto">
            <div class="testimonials-slider">
                <div class="swiper-container">
                    <div class="swiper-wrapper text-center w-50">
                        @for ($i = 1; $i < 6; $i++)
                        <div class="swiper-slide">
                            <div class="d-flex flex-column align-items-center">
                                <img src="{{ asset("img/avatar/" . $i . ".jpg") }}" alt=""
                                    class="rounded-circle shadow mb-4">

                                <p class="w-75 lead mt-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Accusamus asperiores consequatur cum distinctio, dolorem earum error esse ex fugiat
                                    inventore maiores minima, non placeat praesentium quam quas ut, vero voluptatem.</p>
                                <hr class="w-50">
                                <footer>
                                    <cite class="bold text-primary text-uppercase">Jane Doe,</cite>
                                    <p class="small text-secondary mt-0">Awesome Company</p>
                                </footer>
                            </div>
                        </div>
                        @endfor
                    </div>
                </div>

                <div class="swiper-button swiper-button-prev rounded-circle shadow">
                    <i data-feather="arrow-left"></i>
                </div>
                <div class="swiper-button swiper-button-next rounded-circle shadow">
                    <i data-feather="arrow-right"></i>
                </div>
            </div>
        </div>
    </div>
</x-utils.container>
