<!-- ./Slider - default -->
<x-utils.container class="{{ $class ?? '' }}" container-class="{{ $containerClass ?? '' }}">

        <div class="row">
            <div class="col-md-12 mx-auto">
                <div class="swiper-container pb-5" data-sw-nav-arrows=".swiper2-button" data-sw-show-items="2" >
                    <div class="swiper-wrapper">
                        @for ($i = 1; $i < 6; $i++)
                            <div class="swiper-slide">
                                <img src="{{ asset("img/blocks/sliders/{$i}.jpg") }}" alt="" class="img-responsive shadow-lg rounded">
                            </div>
                        @endfor
                    </div>
                    <div class="swiper-pagination"></div>
                    <div class="swiper-button swiper2-button-next swiper-button-next rounded-circle shadow">
                        <i class="fas fa-long-arrow-alt-right"></i>
                    </div>
                    <div class="swiper-button swiper2-button-prev swiper-button-prev rounded-circle shadow">
                        <i class="fas fa-long-arrow-alt-left"></i>
                    </div>
                </div>
            </div>
        </div>
</x-utils.container>
